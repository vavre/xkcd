# XKCD Vavre 👨‍💻

I had a completely different idea when I started this coding challenge.
When reading the description I thought about a Tab Bar controller with a Home pushing to a Detail screen, one with the search functionality and the last one with the favourites.

One of the things I quickly realised was that the images did not have a standard format and were not pretty enough for a good looking collection view. Perhaps a good UI person will come up with better ideas when we are past the MVP phase.

The important part is the functionality and separation of concerns that will allow a future and more mature product to be easily adapted into the current code base.

In the end I opted for a Table View with the comic titles, numbers, favourite icon. ♥️

## 🏡 The Home Controller

- UITableView.
- Modern large titles from iOS11 revamp. 📱
- Cells can be tapped and you will be taken to the Comic Detail screen.
- Cells can also be hard pressed (3D Touch) for a preview of the controller to be displayed. 
- The preview allows you to click to proceed with navigation or share the comic with a friend.
- It contains a search controller that allows you to search by title or Comic number.
- It will display a heart for favourite comics and favourites will be moved to the top for easy access.
- There is also a Pull to Refresh so you can make sure you have the latest Comic

I opted to not implement a network search functionality due to the lack of a good API, the most I could have done was to search by ID only. I though the Filter/Search would be more interesting.

## 🎆 The Comic Detail

The image size for the comic are so different that a fixed size would not be a good solution. I opted to find out the dimensions of the images during download and adjust the height after having the width fill the screen. It needed a scroll view so the image and descriptions could be of any size.

There is an action button in the Comic Details that allows the user to:
- Add and remove from favourites, it will display or hide the heart icon with a cute animation ♥️
- View explanation about the comic, which utilises Safari Services to present the Webpage, it will enable Reader mode if available
- Present the share sheet for the comic

## 🗒 The Data

- In the constant file we set the amount of comics that we need to load for the MVP, ideally this will turn into a endless table view where a set amount of data would be loaded and subsequent only loaded as demand as the user reaches the end of the table view.
- The data is stored locally in the App so it can always be used even without network connection
- For the MVP I took advantage of the Codable protocol and chose to persist data in the UserDefaults, ideally we will move this persistence layer to Core Data or Realm. I've used both in the past but did not have time to implement it now.
- When checking network for new data, we only fetch data that has not yet been downloaded, to avoid unnecessary network calls, the newest found comics will also be persisted.

## 📡 Network monitor

There is a network monitor that will let the user know the internet connection was lost, but he can still see and favourite whatever comics are already in the App.

## 🔦 Desired functionalities Checklist
- ~~browse through the comics~~ ✅ **DONE**
- ~~see the comic details, including its description~~ ✅ **DONE**
- ~~search for comics by the comic number as well as text~~ ✅ **DONE**
- ~~get the comic explanation~~ ✅ **DONE**
- ~~favorite the comics, which would be available offline too~~ ✅ **DONE: In fact everything is available offline**
- ~~send comics to others~~ ✅ **DONE**
- ~~support multiple form factors~~ ✅ **DONE** Supports landscape and portrait modes
- get notifications when a new comic is published ❌

**I thought about creating a serverless function to monitor the Comics website or at least implement the Push Notifications, but I did not have time. I normally use Firebase Cloud Messages for PN.*** 🤓


## 🔨 Unit Tests
When writing Unit Tests I use Quick/Nimble libraries, but as a challenge this time I chose to use the standard Xcode test framework (XCT). I only wrote tests for the Home Presenter, just so you could see my thought process. Besides writing tests to the Presenters I would also have written test for the network clients had I had more time.

## 🕵️‍♂️ The Review

I normally create `feature branches` during development and merge then into `develop` once they are ready. That is what I've done in this MVP as well.
It is good when the VCS history can tell a story on its own, which is pretty much how I like to work. You will notice it.

I opt to use MVP with protocols communication back to the viewController.
View Controllers are inside a folder called Module. I tend to group View Controllers in separated modules with their own files and Storyboard for easy maintenance when a large team is working in the same project.

---
### 🙋‍♂️ Erick Vavretchek // erickva@gmail.com // +47 461 25 030 // 🇧🇷🇦🇺🇳🇴
