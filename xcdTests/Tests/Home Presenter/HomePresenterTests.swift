//
//  HomePresenterTests.swift
//  xcdTests
//
//  Created by Erick Vavretchek on 8/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import XCTest
import Alamofire
@testable import XKCD_Vavre

fileprivate let expectationMainThreadTableView = "Wait for main thread to populate Table View"
fileprivate let expectationBackgroundThreadTableViewUpdate = "Wait for background thread to update Table View"
fileprivate let expectationBackgroundThreadToObtainLatestComic = "Wait for background thread to Obtain Latest Comic"

class HomePresenterTests: XCTestCase {

    fileprivate var viewController: HomeViewControllerMock!
    fileprivate var apiStore: XkcdApiStoreMock!
    var presenter: HomePresenter!
    var testBundle: Bundle!

    override func setUp() {
        testBundle = Bundle(for: type(of: self))
        viewController = HomeViewControllerMock()
        apiStore = XkcdApiStoreMock(testBundle: testBundle)
        presenter = HomePresenter(viewController: viewController, dataSource: HomeDataSource(), store: apiStore, numberOfComicsToFetch: 6)
    }

    func testViewDidLoadColdStart() {
        UserDefaultsClient.shared.clear()
        presenter.viewDidLoad()
        XCTAssert(viewController.stopLoadingCalled, "Stop loading must be triggered as soon as we hit viewDidLoad")
        XCTAssert(viewController.startLoadingCalled, "Start loading must be called when there is no data persisted")
        let expectationMainThread = self.expectation(description: expectationMainThreadTableView)
        viewController.expectations.append(expectationMainThread)
        wait(for: [expectationMainThread], timeout: 1)
        XCTAssert(self.viewController.refreshTableCalled, "Table View must be refreshed after new data is loaded")
        XCTAssert(UserDefaultsClient.shared.comics.count == 6, "There are 6 comics and all are expected to be persisted")
        XCTAssert(presenter.dataSource.comics.count == 6, "Table View Data Source expected to have 6 comics")
        XCTAssert(presenter.unfilteredComics.count == 6, "List of unfilteres comics must have the total loaded comics so it can be used for search")
    }
    
    func testViewDidLoadExistingData() {
        createPersistedComics()
        presenter.viewDidLoad()
        XCTAssert(viewController.stopLoadingCalled, "Stop loading must be triggered as soon as we hit viewDidLoad")
        XCTAssert(self.viewController.refreshTableCalled, "Table View must be refreshed after new data is loaded")
        let expectationBackgroundThread = self.expectation(description: expectationBackgroundThreadTableViewUpdate)
        viewController.expectations.append(expectationBackgroundThread)
        wait(for: [expectationBackgroundThread], timeout: 1)
        XCTAssert(UserDefaultsClient.shared.comics.count == 6, "There were 3 new comics found online, bringing the total count to 6")
        XCTAssert(presenter.unfilteredComics.count == 6, "List of unfilteres comics must have the total loaded comics so it can be used for search")
    }
    
    func testNavigationToComicDetailRequest() {
        UserDefaultsClient.shared.clear()
        createPersistedComics()
        presenter.dataSource.comics = UserDefaultsClient.shared.comics
        presenter.didSelectRowRequest(index: 0)
        XCTAssert(viewController.comicTitle == "Title 1", "We expect navigation to visit screen for comic with title Title 1")
    }
    
    func testComicSharingRequest() {
        presenter.shareComicRequest(comicId: 3)
        XCTAssert(viewController.sharingComicWithId == 3, "Comic Id of selected comic to share must be 3")
    }
    
    func testPullToRefreshRequest() {
        createPersistedComics()
        presenter.pullToRefreshRequest()
        let expectationBackgroundThread = self.expectation(description: expectationBackgroundThreadTableViewUpdate)
        viewController.expectations.append(expectationBackgroundThread)
        wait(for: [expectationBackgroundThread], timeout: 1)
        XCTAssert(UserDefaultsClient.shared.comics.count == 6, "There were 3 new comics found online, bringing the total count to 6")
        XCTAssert(presenter.unfilteredComics.count == 6, "List of unfilteres comics must have the total loaded comics so it can be used for search")
    }
    
    func testFilterComicsRequest() {
        createPersistedComics()
        presenter.unfilteredComics = UserDefaultsClient.shared.comics
        presenter.filterComicsRequest(with: "2")
        XCTAssert(presenter.dataSource.comics.count == 1, "We must have only 1 item filterd with number 2")
        XCTAssert(presenter.dataSource.comics[0].title == "Title 2", "Comic with Title 2 must have been filtered")
        presenter.filterComicsRequest(with: "Testing")
        XCTAssert(presenter.dataSource.comics.count == 0, "We must have 0 item filterd with string Testing")
    }
    
    func testErrorMessageWhenNetworkFail() {
        createPersistedComics()
        apiStore.shouldFailRequests = true
        presenter.pullToRefreshRequest()
        let expectationBackgroundThreadLatestComic = self.expectation(description: expectationBackgroundThreadToObtainLatestComic)
        viewController.expectations.append(expectationBackgroundThreadLatestComic)
        wait(for: [expectationBackgroundThreadLatestComic], timeout: 1)
        XCTAssert(viewController.displayErrorMessageCalled, "Error message expected after failed network request")
    }
    
    private func createPersistedComics() {
        let comics = testBundle.decode([Comic].self, from: "SampleListOfComics.json")
        UserDefaultsClient.shared.comics = [comics[0], comics[1], comics[2]]
    }
}

fileprivate class HomeViewControllerMock: HomeViewControllerProtocol {
    
    var expectations = [XCTestExpectation]()
    
    var startLoadingCalled = false
    func startLoading() {
        startLoadingCalled = true
    }
    
    var stopLoadingCalled = false
    func stopLoading() {
        stopLoadingCalled = true
    }
    
    var refreshTableCalled = false
    func refreshTable() {
        let expectation = expectations.first {$0.description == expectationMainThreadTableView}
        guard let foundExpectation = expectation else {
            refreshTableCalled = true
            return
        }
        refreshTableCalled = true
        foundExpectation.fulfill()
    }
    
    var displayErrorMessageCalled = false
    func displayErrorMessage(_ message: String) {
        let expectation = expectations.first {$0.description == expectationBackgroundThreadToObtainLatestComic}
        guard let foundExpectation = expectation else {
            refreshTableCalled = true
            return
        }
        displayErrorMessageCalled = true
        foundExpectation.fulfill()
    }
    
    var updateTableWithNewComicsCalled = false
    func updateTableWithNewComics(numberOfNewComics: Int) {
        let expectation = expectations.first {$0.description == expectationBackgroundThreadTableViewUpdate}
        guard let foundExpectation = expectation else {
            return
        }
        updateTableWithNewComicsCalled = true
        foundExpectation.fulfill()
    }
    
    var comicTitle: String? = nil
    func navigateToComicDetail(with comic: Comic) {
        comicTitle = comic.title
    }
    
    var sharingComicWithId: Int? = nil
    func shareComic(comicId: Int) {
        sharingComicWithId = comicId
    }
}

fileprivate class XkcdApiStoreMock: XkcdApiStore {
    var shouldFailRequests = false
    let testBundle: Bundle
    init(testBundle: Bundle) { self.testBundle = testBundle }
    
    override func getLatestComic(completionHandler: @escaping (Result<Comic, AFError>) -> Void) {
        if shouldFailRequests {
            completionHandler(Result.failure(AFError.explicitlyCancelled))
        } else {
            let latestComic = testBundle.decode(Comic.self, from: "LatestComic.json")
            completionHandler(Result.success(latestComic))
        }
    }
    
    override func getComic(id: Int, completionHandler: @escaping (Result<Comic, AFError>) -> Void) {
        if shouldFailRequests {
            completionHandler(Result.failure(AFError.explicitlyCancelled))
        } else {
            let comics = testBundle.decode([Comic].self, from: "SampleListOfComics.json")
            let comic = comics[id - 1]
            completionHandler(Result.success(comic))
        }
    }
}
