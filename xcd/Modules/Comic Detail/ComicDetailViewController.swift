//
//  ComicDetailViewController.swift
//  xcd
//
//  Created by Erick Vavretchek on 6/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import UIKit
import Kingfisher
import SafariServices

class ComicDetailViewController: UIViewController, Storyboarded, CanPresentMessagesProtocol {
    
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favouriteImageView: UIImageView!
    @IBOutlet weak var comicImageView: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var issueLabel: UILabel!
    @IBOutlet weak var altTextView: UITextView!
    
    var presenter: ComicDetailPresenter!
    var imageHeight: CGFloat?
    var imageWidth: CGFloat?
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.viewDidLoad()
        prepareUiElements()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        adjustImageHeight(orientationChanged: true)
    }
    
    // MARK: Private
    private func prepareUiElements() {
        let actionBarButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(actionBarButtonAction))
        navigationItem.rightBarButtonItem = actionBarButton
        preferredContentSize = CGSize(width: UIScreen.main.bounds.width - 40, height: 400) // This is the preferred size for Popover menu
    }
    
    @objc  private func actionBarButtonAction() {
        presenter.actionBarButtonRequest()
    }
    
    private func adjustImageHeight(orientationChanged: Bool) {
        guard let width = imageWidth, let height = imageHeight else { return }
        let imageViewWidth = orientationChanged ? UIScreen.main.bounds.height - 40 : UIScreen.main.bounds.width - 40
        let calculatedHeight = imageViewWidth * height / width
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.imageHeightConstraint.constant = calculatedHeight
            self?.view.layoutIfNeeded()
        }
    }
    
    private func updateProgressView(to value: Float) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: { [weak self] in
            self?.progressView.setProgress(value, animated: true)
        }) { isComplete in
            if isComplete && value == 1.0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
                    UIView.animate(withDuration: 0.5) { [weak self] in
                        self?.progressView.alpha = 0
                    }
                }
            }
        }
    }
}

// MARK: ComicDetailViewControllerProtocol
extension ComicDetailViewController: ComicDetailViewControllerProtocol {
    func setComicDetails(with comic: Comic) {
        title = "ISSUE #\(comic.id)"
        titleLabel.text = comic.title
        issueLabel.text = "ISSUE #\(comic.id)"
        altTextView.text = comic.alt
        favouriteImageView.isHidden = !comic.isFavourite
        comicImageView.kf.indicatorType = .activity
        comicImageView.kf.setImage(with: comic.image, placeholder: nil, options: [.backgroundDecode], progressBlock: { [weak self] (receivedSize, totalSize) in
            self?.updateProgressView(to: Float(receivedSize)/Float(totalSize))
        }) { [weak self] result in
            switch result {
            case .success(let value):
                self?.imageWidth = value.image.size.width
                self?.imageHeight = value.image.size.height
                self?.adjustImageHeight(orientationChanged: false)
            case .failure(let error):
                if case let KingfisherError.responseError(reason: responseError) = error,
                   case let KingfisherError.ResponseErrorReason.URLSessionError(error: errorReason) = responseError {
                    self?.displayToast(errorReason.localizedDescription, withTitle: "Error", type: .error)
                } else {
                    self?.displayToast("Error downloading image.", withTitle: "Error", type: .error)
                }
            }
        }
    }
    
    func presentActionsDialog() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let isFavourite = presenter.comic.isFavourite
        let favoritesTitle = isFavourite ? "Remove from favourites" : "Add to favourites"
        let favouriteStyle = isFavourite ? UIAlertAction.Style.destructive : UIAlertAction.Style.default
        let favoriteAction = UIAlertAction(title: favoritesTitle, style: favouriteStyle) { _ in
            self.presenter.toggleFavouriteRequest()
        }
        
        let viewExplanationAction = UIAlertAction(title: "View Explanation", style: .default) { _ in
            self.presenter.viewExplanationRequest()
        }
        
        let shareAction = UIAlertAction(title: "Share Comic", style: .default) { _ in
            self.presenter.shareComicRequest()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(favoriteAction)
        alertController.addAction(viewExplanationAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func navigateToExplanation(for comicId: Int) {
        if let url = URL(string: "https://www.explainxkcd.com/wiki/index.php/\(comicId)") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
    func updateFavouriteIcon() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success) // signal success haptic feedback
        
        if presenter.comic.isFavourite {
            favouriteImageView.isHidden = false
            favouriteImageView.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
            UIView.animate(withDuration: 0.8, delay: 0.1, usingSpringWithDamping: 0.4, initialSpringVelocity: 10, options: .curveEaseIn, animations: { [weak self] in
                self?.favouriteImageView.transform = CGAffineTransform.identity
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.6, delay: 0.1, options: .curveEaseOut, animations: { [weak self] in
                self?.favouriteImageView.alpha = 0
            }) { [weak self] _ in
                self?.favouriteImageView.isHidden = true
                self?.favouriteImageView.alpha = 1
            }
        }
    }
    
    func shareComic(comicId: Int) {
        let items = [URL(string: "https://xkcd.com/\(comicId)")!]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        self.present(ac, animated: true)
    }
}
