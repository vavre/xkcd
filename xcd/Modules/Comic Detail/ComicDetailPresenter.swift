//
//  ComicDetailPresenter.swift
//  xcd
//
//  Created by Erick Vavretchek on 6/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation

protocol ComicDetailViewControllerProtocol: class {
    func setComicDetails(with comic: Comic)
    func presentActionsDialog()
    func navigateToExplanation(for comicId: Int)
    func updateFavouriteIcon()
    func shareComic(comicId: Int)

}

class ComicDetailPresenter {
    unowned let viewController: ComicDetailViewControllerProtocol
    var comic: Comic
    
    init(viewController: ComicDetailViewControllerProtocol,
         comic: Comic) {
        self.viewController = viewController
        self.comic = comic
    }
    
    func viewDidLoad() {
        viewController.setComicDetails(with: comic)
    }
    
    // MARK: Requests
    func actionBarButtonRequest() {
        viewController.presentActionsDialog()
    }
    
    func viewExplanationRequest() {
        viewController.navigateToExplanation(for: comic.id)
    }
    
    func toggleFavouriteRequest() {
        comic.toggleFavourite()
        viewController.updateFavouriteIcon()
    }
    
    func shareComicRequest() {
        viewController.shareComic(comicId: comic.id)
    }
}
