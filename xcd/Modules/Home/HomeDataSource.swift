//
//  HomeDataSource.swift
//  xcd
//
//  Created by Erick Vavretchek on 6/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation

import UIKit

class HomeDataSource: NSObject, UITableViewDataSource {
    
    var comics = [Comic]()
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeCell.reuseIdentifier, for: indexPath) as? HomeCell else {
            return UITableViewCell()
        }
        cell.configure(comic: comics[indexPath.row])
        return cell
    }
}

