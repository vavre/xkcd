//
//  HomeCell.swift
//  xcd
//
//  Created by Erick Vavretchek on 6/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var issueLabel: UILabel!
    @IBOutlet weak var favouriteImageView: UIImageView!
    
    static let reuseIdentifier = "HomeCell"

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(comic: Comic) {
        titleLabel.text = comic.title
        issueLabel.text = "Issue #\(comic.id)"
        favouriteImageView.isHidden = !comic.isFavourite
    }
}
