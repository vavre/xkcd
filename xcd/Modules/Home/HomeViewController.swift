//
//  HomeViewController.swift
//  xcd
//
//  Created by Erick Vavretchek on 6/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController, CanPresentMessagesProtocol {
    
    var presenter: HomePresenter!
    let activityIndicator = UIActivityIndicatorView(style: .medium)
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUiElements()
        presenter = HomePresenter(viewController: self)
        tableView.dataSource = presenter.dataSource
        presenter.viewDidLoad()
    }
    
    // MARK: Actions
    @objc private func pullToRefreshAction() {
        presenter.pullToRefreshRequest()
    }
    
    // MARK: Private
    private func prepareUiElements() {
        title = "XKCD Collection"
        addActivityIndicatorView()
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(pullToRefreshAction), for: .valueChanged)
        tableView.tableFooterView = UIView()
        let searchController = UISearchController()
        searchController.automaticallyShowsCancelButton = true
        searchController.searchBar.placeholder = "Search by Title or Issue"
        searchController.searchBar.searchTextField.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
    }
    
    private func addActivityIndicatorView() {
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.color = .black
        let horizontalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        view.addConstraint(horizontalConstraint)
        let verticalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        view.addConstraint(verticalConstraint)
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowRequest(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let comic = presenter.dataSource.comics[indexPath.row]
        
        let viewComic = UIAction(title: "View Comic", image: UIImage(systemName: "eye")) {_ in
            self.presenter.didSelectRowRequest(index: indexPath.row)
        }
        
        let shareComic = UIAction(title: "Share Comic", image: UIImage(systemName: "square.and.arrow.up")) {_ in
            self.presenter.shareComicRequest(comicId: comic.id)
        }
        
        return UIContextMenuConfiguration(identifier: nil, previewProvider: { () -> UIViewController? in
            let vc = ComicDetailViewController.instantiate()
            let vcPresenter = ComicDetailPresenter(viewController: vc, comic: self.presenter.dataSource.comics[indexPath.row])
            vc.presenter = vcPresenter
            return vc
        }) { _ in
            UIMenu(title: "", children: [viewComic, shareComic])
        }
    }
}

// MARK:: HomeViewControllerProtocol
extension HomeViewController: HomeViewControllerProtocol {
    func startLoading() {
        activityIndicator.isHidden = false
    }
    
    func stopLoading() {
        activityIndicator.isHidden = true
        tableView.refreshControl?.endRefreshing()
    }
    
    func refreshTable() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    func displayErrorMessage(_ message: String) {
        displayToast(message, withTitle: "Error", type: .error)
    }
    
    func updateTableWithNewComics(numberOfNewComics: Int) {
        var indexPaths = [IndexPath]()
        for row in 0..<numberOfNewComics {
            indexPaths.append(IndexPath(row: row, section: 0))
        }
        tableView.insertRows(at: indexPaths, with: .bottom)
    }
    
    func navigateToComicDetail(with comic: Comic) {
        let vc = ComicDetailViewController.instantiate()
        let presenter = ComicDetailPresenter(viewController: vc, comic: comic)
        vc.presenter = presenter
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func shareComic(comicId: Int) {
        let items = [URL(string: "https://xkcd.com/\(comicId)")!]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        self.present(ac, animated: true)
    }
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        presenter.filterComicsRequest(with: textField.text ?? "")
    }
}
