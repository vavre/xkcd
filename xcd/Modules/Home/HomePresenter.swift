//
//  HomePresenter.swift
//  xcd
//
//  Created by Erick Vavretchek on 6/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation

protocol HomeViewControllerProtocol: class {
    func startLoading()
    func stopLoading()
    func refreshTable()
    func displayErrorMessage(_ message: String)
    func updateTableWithNewComics(numberOfNewComics: Int)
    func navigateToComicDetail(with comic: Comic)
    func shareComic(comicId: Int)
}

class HomePresenter {
    unowned let viewController: HomeViewControllerProtocol
    let dataSource: HomeDataSource
    let store: XkcdApiStore
    let numberOfComicsToFetch: Int
    var latestComic: Comic?
    var unfilteredComics = [Comic]()
    
    init(viewController: HomeViewControllerProtocol,
         dataSource: HomeDataSource = HomeDataSource(),
         store: XkcdApiStore = XkcdApiStore(),
         numberOfComicsToFetch: Int = Constants.DefaultValues.numberOfComicsToFetch) {
        self.viewController = viewController
        self.dataSource = dataSource
        self.store = store
        self.numberOfComicsToFetch = numberOfComicsToFetch
    }
    
    func viewDidLoad() {
        viewController.stopLoading()
        loadComics()
        setObserverForDataSource()
    }
    
    // MARK: Requests
    func pullToRefreshRequest() {
        checkForNewComics()
    }
    
    func didSelectRowRequest(index: Int) {
        viewController.navigateToComicDetail(with: dataSource.comics[index])
    }
    
    func shareComicRequest(comicId: Int) {
        viewController.shareComic(comicId: comicId)
    }
    
    func filterComicsRequest(with text: String) {
        if text.isEmpty {
            populateDataSource(comics: unfilteredComics)
        } else {
            let filteredComics = unfilteredComics.filter {
                $0.title.lowercased().contains(text.lowercased()) || "\($0.id)".contains(text)
            }
            populateDataSource(comics: filteredComics)
        }
    }
    
    var newFecthedComics: [Comic]? {
        didSet {
            guard newFecthedComics != nil else { return }
            let savedComics = UserDefaultsClient.shared.comics
            var updatedComics = savedComics
            updatedComics.insert(contentsOf: newFecthedComics!, at: 0)
            updatedComics.sort { (c1, c2) -> Bool in
                if c1.isFavourite == c2.isFavourite {
                   return c1.id > c2.id
                }
                return c1.isFavourite && !c2.isFavourite
            }
            persistComics(updatedComics)
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                if savedComics.isEmpty {
                    self.viewController.stopLoading()
                    self.populateDataSource(comics: updatedComics)
                } else {
                    self.dataSource.comics = updatedComics
                    self.viewController.updateTableWithNewComics(numberOfNewComics: self.newFecthedComics!.count)
                }
            }
        }
    }
    
    // MARK: Private
    private func loadComics() {
        let persistedComics = UserDefaultsClient.shared.comics
        if persistedComics.isEmpty {
            viewController.startLoading()
            store.getLatestComic { [weak self] result in
                switch result {
                case .success(let comic):
                    self?.latestComic = comic
                    self?.fetchSubsequentComicList(from: comic)
                case .failure(let error):
                    DispatchQueue.main.async { [weak self] in
                        self?.viewController.stopLoading()
                        self?.viewController.displayErrorMessage(error.localizedDescription)
                    }
                }
            }
        } else {
            populateDataSource(comics: persistedComics)
            unfilteredComics = persistedComics
            checkForNewComics()
        }
    }
    
    private func fetchSubsequentComicList(from newestComic: Comic, to lastComicId: Int? = nil) {
        let firstComicIdToFetch = newestComic.id - 1
        let lastComicIdToFetch = lastComicId == nil ? newestComic.id - numberOfComicsToFetch + 1 : lastComicId!
        
        let group = DispatchGroup()
        var comics = [newestComic]
        
        for i in stride(from: firstComicIdToFetch, through: lastComicIdToFetch, by: -1) {
            group.enter()
            store.getComic(id: i) { result in
                if case let Result.success(fetchedComic) = result {
                    comics.append(fetchedComic)
                }
                group.leave()
            }
        }
        
        group.notify(queue: .global(qos: .userInitiated)) { [weak self] in
            comics.sort {$0.id > $1.id}
            self?.newFecthedComics = comics
        }
    }
    
    private func persistComics(_ comics: [Comic]) {
        UserDefaultsClient.shared.comics = comics
        unfilteredComics = comics
    }
    
    private func populateDataSource(comics: [Comic]) {
        dataSource.comics = comics
        viewController.refreshTable()
    }
    
    private func checkForNewComics() {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let latestComicIdSaved = UserDefaultsClient.shared.comics.max { $0.id < $1.id }?.id
            guard latestComicIdSaved != nil else {
                self?.viewController.stopLoading()
                return
            }
            self?.store.getLatestComic { [weak self] result in
                switch result {
                case .success(let latestRetrievedComic):
                    if latestRetrievedComic.id != latestComicIdSaved {
                        self?.fetchSubsequentComicList(from: latestRetrievedComic, to: latestComicIdSaved! + 1)
                    } else {
                        self?.viewController.stopLoading()
                    }
                case .failure(let error):
                    self?.viewController.stopLoading()
                    self?.viewController.displayErrorMessage(error.localizedDescription)
                }
            }
        }
    }
    
    private func setObserverForDataSource() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateDataSourceFromPersistenceData), name: NSNotification.Name(Constants.NotificationCenter.comicsUpdated), object: nil)
    }
    
    @objc private func updateDataSourceFromPersistenceData() {
        let savedComics = UserDefaultsClient.shared.comics
        populateDataSource(comics: savedComics)
    }
}
