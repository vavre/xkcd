//
//  CanPresentMessagesProtocol.swift
//  xcd
//
//  Created by Erick Vavretchek on 7/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation
import SwiftMessages

protocol CanPresentMessagesProtocol {
    func displayToast(_ message: String, withTitle title: String, type: Theme)
}

extension CanPresentMessagesProtocol {
    func displayToast(_ message: String, withTitle title: String, type: Theme) {
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(type)
        view.configureDropShadow()
        view.button?.isHidden = true
        view.configureContent(title: title, body: message)
        DispatchQueue.main.async {
            SwiftMessages.show(view: view)
        }
    }
    
    func displayFixedToast(_ message: String, withTitle title: String, type: Theme, callToAction: String?, action: ((UIButton) -> Void)?) {
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(type)
        view.configureDropShadow()

        if callToAction != nil {
            view.button?.setTitle(callToAction!, for: .normal)
            view.buttonTapHandler = action
        } else {
            view.button?.isHidden = true
        }
        
        view.tapHandler = { _ in
            action?(UIButton())
            SwiftMessages.hide()
        }
        
        view.configureContent(title: title, body: message)
        var config = SwiftMessages.Config()
        config.duration = .forever
        SwiftMessages.show(config: config, view: view)
    }
}
