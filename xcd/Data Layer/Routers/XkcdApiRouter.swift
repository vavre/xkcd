//
//  XkcdApiRouter.swift
//  xcd
//
//  Created by Erick Vavretchek on 5/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation
import Alamofire

enum XkcdApiRouter: XkcdApiRouterProtocol {
    case getLatestComic
    case getComic(id: Int)
    
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .getLatestComic:
            return "info.0.json"
        case .getComic(let id):
            return "\(id)/info.0.json"
        }
    }
}
