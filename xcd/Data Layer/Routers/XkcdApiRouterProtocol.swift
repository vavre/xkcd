//
//  XkcdApiRouterProtocol.swift
//  xcd
//
//  Created by Erick Vavretchek on 5/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import UIKit
import Alamofire

protocol XkcdApiRouterProtocol: ApiRouterProtocol {}

extension XkcdApiRouterProtocol {
    
    var baseURL: URL {
        return AppConfigStore.shared.xkcdApiBaseUrl
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: true)
        urlComponents?.path = "/\(path)"
        if queryString != nil {
            urlComponents?.query = queryString
        }
        
        let url = urlComponents?.url
        var urlRequest = URLRequest(url: url!)
        
        urlRequest.httpMethod = method.rawValue
        
        for (headerField, value) in headers ?? [:] {
            urlRequest.setValue(value, forHTTPHeaderField: headerField)
        }
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    var encoding: ParameterEncoding {
        switch method {
        case .post:
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }
}
