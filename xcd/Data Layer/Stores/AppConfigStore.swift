//
//  AppConfigStore.swift
//  xcd
//
//  Created by Erick Vavretchek on 5/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation

class AppConfigStore {
    private init() {}
    static let shared = AppConfigStore()
    
    private var environment: Environment {
        #if DEBUG
        return .dev
        #else
        return .prod
        #endif
    }
    
    var xkcdApiBaseUrl: URL {
        return environment.xkcdApiBaseUrl
    }
    
    var networkTimeout: TimeInterval {
        return 60
    }
}
