//
//  XkcdApiStore.swift
//  xcd
//
//  Created by Erick Vavretchek on 5/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation
import Alamofire

class XkcdApiStore {
    let client: XkcdApiClient
    init(client: XkcdApiClient = XkcdApiClient()) {
        self.client = client
    }
    
    func getLatestComic(completionHandler: @escaping (Result<Comic, AFError>) -> Void) {
        client.getLatestComic { response in
            completionHandler(response.result)
        }
    }
    
    func getComic(id: Int, completionHandler: @escaping (Result<Comic, AFError>) -> Void) {
        client.getComic(id: id) { response in
            completionHandler(response.result)
        }
    }
}
