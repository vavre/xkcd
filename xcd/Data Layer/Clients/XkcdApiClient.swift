//
//  XkcdApiClient.swift
//  xcd
//
//  Created by Erick Vavretchek on 5/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation
import Alamofire

class XkcdApiClient {
    
    let session = NetworkSessionManager.shared.sessionManager
    
    func getLatestComic(completionHandler: @escaping (DataResponse<Comic, AFError>) -> Void) {
        session.request(XkcdApiRouter.getLatestComic)
        .validate()
        .responseDecodable(completionHandler: completionHandler)
    }
    
    func getComic(id: Int, completionHandler: @escaping (DataResponse<Comic, AFError>) -> Void) {
        session.request(XkcdApiRouter.getComic(id: id))
        .validate()
        .responseDecodable(completionHandler: completionHandler)
    }
}
