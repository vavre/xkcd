//
//  Constants.swift
//  xcd
//
//  Created by Erick Vavretchek on 6/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation

struct Constants {
    struct UserDefaults {
        static let comics = "comics"
    }
    
    struct NotificationCenter {
        static let comicsUpdated = "comicsUpdated"
    }
    
    struct DefaultValues {
        static let numberOfComicsToFetch = 20
    }
}
