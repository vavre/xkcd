//
//  UserDefaultsClient.swift
//  xcd
//
//  Created by Erick Vavretchek on 6/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation

class UserDefaultsClient {
    static let shared = UserDefaultsClient()
    
    private var userDefaults: UserDefaults {
        #if DEBUG
        if ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil {
            return UserDefaults.init(suiteName: "XcodeTest")!
        } else {
            return UserDefaults.standard
        }
        #else
        return UserDefaults.standard
        #endif
    }
    
    func clear() {
        comics = []
    }
    
    var comics: [Comic] {
        get {
            guard let savedComics = userDefaults.object(forKey: Constants.UserDefaults.comics) as? Data else { return [] }
            let decodedComics = try? JSONDecoder().decode([Comic].self, from: savedComics)
            return decodedComics ?? []
        }
        
        set {
            guard let encoded = try? JSONEncoder().encode(newValue) else { return }
            userDefaults.set(encoded, forKey: Constants.UserDefaults.comics)
        }
    }
}
