//
//  Comic.swift
//  xcd
//
//  Created by Erick Vavretchek on 5/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation

struct Comic: Codable {
    let id: Int
    let title: String
    let image: URL
    let alt: String
    var isFavourite: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "num"
        case title = "safe_title"
        case image = "img"
        case alt
        case isFavourite
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        image = try container.decode(URL.self, forKey: .image)
        alt = try container.decode(String.self, forKey: .alt)
        isFavourite = (try? container.decode(Bool.self, forKey: .isFavourite)) ?? false
    }
    
    mutating func toggleFavourite() {
        self.isFavourite = !self.isFavourite
        save()
    }
    
    func save() {
        DispatchQueue.global(qos: .userInitiated).async {
            var savedComics = UserDefaultsClient.shared.comics
            if let foundIndex = savedComics.firstIndex(where: {$0.id == self.id}) {
                savedComics[foundIndex] = self
                savedComics.sort { (c1, c2) -> Bool in
                    if c1.isFavourite == c2.isFavourite {
                        return c1.id > c2.id
                    }
                    return c1.isFavourite && !c2.isFavourite
                }
                UserDefaultsClient.shared.comics = savedComics
                self.postNotification()
            }
        }
    }
    
    // MARK: Private
    private func postNotification() {
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationCenter.comicsUpdated), object: self)
    }
}
