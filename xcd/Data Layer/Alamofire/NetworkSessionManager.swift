//
//  NetworkSessionManager.swift
//  xcd
//
//  Created by Erick Vavretchek on 5/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation
import Alamofire

class NetworkSessionManager {
    private init() {}
    static let shared = NetworkSessionManager()
    
    let sessionManager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = AppConfigStore.shared.networkTimeout
        configuration.timeoutIntervalForResource = AppConfigStore.shared.networkTimeout
        let session = Session(configuration: configuration)
        return session
    }()
}

