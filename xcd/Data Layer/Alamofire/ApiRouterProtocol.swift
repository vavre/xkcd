//
//  ApiRouterProtocol.swift
//  xcd
//
//  Created by Erick Vavretchek on 5/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation
import Alamofire

protocol ApiRouterProtocol: URLRequestConvertible {
    var baseURL: URL { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
    var encoding: ParameterEncoding { get }
    var headers: [String: String]? { get }
    var queryString: String? { get }
}

extension ApiRouterProtocol {
    var parameters: Parameters? {
        return nil
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var queryString: String? {
        return nil
    }
}

