//
//  Environment.swift
//  xcd
//
//  Created by Erick Vavretchek on 5/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import Foundation

enum Environment: String {
    case dev, prod
    
    var xkcdApiBaseUrl: URL {
        switch self {
        case .prod, .dev:
            return URL(string: "https://xkcd.com")!
        }
    }
}
