//
//  AppDelegate.swift
//  xcd
//
//  Created by Erick Vavretchek on 5/2/20.
//  Copyright © 2020 Erick Vavretchek. All rights reserved.
//

import UIKit
import Network
import SwiftMessages

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CanPresentMessagesProtocol {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        monitorInternetAccess()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    private func monitorInternetAccess() {
        let monitor = NWPathMonitor()
        monitor.pathUpdateHandler = { path in
            if path.status == .unsatisfied {
                self.displayNoConnectionMessage()
            }
        }
        monitor.start(queue: DispatchQueue(label: "Monitor"))
    }
    
    private func displayNoConnectionMessage() {
        DispatchQueue.main.async {
            self.displayFixedToast("But you will still be able to use the App offline with the comics you had previously downloaded.", withTitle: "No Internet Connection", type: .info, callToAction: nil, action: nil)
        }
    }
}
